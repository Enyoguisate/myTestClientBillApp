﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testClientBillDAL;
using testClientBillModels;

namespace testClientBillBLL
{
    public class Clients
    {
        private testClientBillDAL.Clients _clientDal;

        public Clients()
        {
            _clientDal = new testClientBillDAL.Clients();
        }

        public List<testClientBillModels.Clients> GetClientList(int id)
        {
            return _clientDal.GetClientsById(id);
        }

        public bool RegisterClient(testClientBillModels.Clients client)
        {
            return _clientDal.InsertClient(client);
        }

        public bool ChangeClient(testClientBillModels.Clients client)
        {
            return _clientDal.UpdateClient(client);
        }

        public bool RemoveClient(int idClient)
        {
            return _clientDal.DeleteClientById(idClient);
        }

        
    }
}
