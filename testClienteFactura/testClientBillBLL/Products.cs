﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testClientBillDAL;

namespace testClientBillBLL
{
    public class Products
    {
        private testClientBillDAL.Products _productsDal;

        public Products()
        {
            _productsDal = new testClientBillDAL.Products();
        }

        public List<testClientBillModels.Products> GetProductsById(int idProduct)
        {
            return _productsDal.GetProductsById(idProduct);
        }
    }
}
