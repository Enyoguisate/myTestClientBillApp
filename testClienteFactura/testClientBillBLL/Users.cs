﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testClientBillModels;
using testClientBillDAL;

namespace testClientBillBLL
{
    public class Users
    {
        private testClientBillDAL.Users _usersDal;

        public Users()
        {
            _usersDal = new testClientBillDAL.Users();
        }

        public bool RegisterUser(testClientBillModels.Users user)
        {
            return _usersDal.InsertUser(user);
        } 

    }
}
