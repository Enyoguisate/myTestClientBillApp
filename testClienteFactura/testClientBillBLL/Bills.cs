﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testClientBillModels;
using testClientBillDAL;

namespace testClientBillBLL
{
    public class Bills
    {
        private testClientBillDAL.Bills _billDal;

        public Bills()
        {
            _billDal = new testClientBillDAL.Bills();
        }

        public List<testClientBillModels.Bills> GetBillsList(int idBill)
        {
            return _billDal.GetBillsById(idBill,0,0);
        }

        public List<testClientBillModels.Bills> GetBillsListByClientId(int idClient)
        {
            return _billDal.GetBillsById(0, idClient,1);
        }

        public bool RegistarBill(testClientBillModels.Bills bill)
        {
            return _billDal.InsertBill(bill);
        }

        public bool ChangeBill(testClientBillModels.Bills bill)
        {
            return _billDal.UpdateBill(bill);
        }

        public bool RemoveBill(int idBill)
        {
            return _billDal.DeleteBillById(idBill);
        }

        public List<testClientBillModels.ClientBill> GetClientBills(int idClient, int idBill)
        {
            int op = 0;
            if (idClient == 0 && idBill == 0) op = 1;
            return _billDal.GetClientBillsById(idClient, idBill, op);
        }

        public bool RegisterClientBill(ClientBill clientBill)
        {
            testClientBillModels.Bills bill = new testClientBillModels.Bills
            {
                IdClient = clientBill.IdClient,
                BillDate = DateTime.Today,
                BillNumber = _billDal.GetLastBillNumber(),
                BillTotal = clientBill.BillTotal
            };
            return _billDal.InsertBill(bill);

        }

        public bool ModifyClientBill(ClientBill clientBill)
        {
            testClientBillModels.Bills bill = new testClientBillModels.Bills
            {
                IdBill = clientBill.IdBill,
                IdClient = clientBill.IdClient,
                BillDate = DateTime.Today,
                BillNumber = clientBill.BillNumber,
                BillTotal = clientBill.BillTotal
            };
            return _billDal.UpdateBill(bill);
        }

        public int GetLastBillNumber()
        {
            return _billDal.GetLastBillNumber();
        }

    }
}
