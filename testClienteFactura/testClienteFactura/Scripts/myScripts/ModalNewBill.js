﻿// Modal new bill
var clientSelected = false;
var detailAdded = false;
var page = 1;
$(document).ready(function () {
    $('#modalNewBillOpen').on('click', function () {
        $.ajax({
            url: '/bills/GetProducts',
            type: 'GET',
            data: { 'page': page },
            error: function (ex) {
                console.log(ex);
            },
            success: function (response) {
                if (response.Results != null) {
                    var jsonResponse = response.Results;
                    console.log(jsonResponse);
                    var tableStr = "";
                    for (var i = 0; i < jsonResponse.length; i++) {
                        tableStr += '<tr>';
                        tableStr += '<td class="hidden">' + jsonResponse[i].IdProduct + '</td>';
                        tableStr += '<td>' + jsonResponse[i].Name + '</td>';
                        tableStr += '<td>' + jsonResponse[i].Price + '</td>';
                        tableStr += '<td>' + jsonResponse[i].Stock + '</td>';
                        tableStr += '</tr>';
                    }
                    $('#modalNewBillAddProductsTable > tbody ').append(tableStr);
                }
            }
        });
    });
});

//btn pagination
/* id="firstPage"> id="previousPage" id="pageOne" id="pageTwo" id="pageThree" id="pageFour" id="pageFive" id="nextPage"> id="lastPage"> */
$(document).ready(function () {
    $('#modalNewBillOpen').on('click', function () {
        $('#firstPage').on('click', function () {
            clearProductsTable();
            page = 1;
            GetProducts(page);
        });
        $('#lastPage').on('click', function () {
            clearProductsTable();
            page = 5;
            GetProducts(page);
        });
        $('#pageOne').on('click', function () {
            clearProductsTable();
            page = 1;
            GetProducts(page);
        });
        $('#pageTwo').on('click', function () {
            clearProductsTable();
            page = 2;
            GetProducts(page);
        });
        $('#pageThree').on('click', function () {
            clearProductsTable();
            page = 3;
            GetProducts(page);
        });
        $('#pageFour').on('click', function () {
            clearProductsTable();
            page = 4;
            GetProducts(page);
        });
        $('#pageFive').on('click', function () {
            clearProductsTable();
            page = 5;
            GetProducts(page);
        });
        $('#previousPage').on('click', function () {
            clearProductsTable();
            if (page > 1) page--;
            GetProducts(page);
        });
        $('#nextPage').on('click', function () {
            clearProductsTable();
            if (page < 5) page++;
            GetProducts(page);
        });
    });
});


function clearProductsTable() {
    $('#modalNewBillAddProductsTable tr td').each(function () {
        $(this).remove();
    });
}

function GetProducts(page) {
    $.ajax({
        url: '/bills/GetProducts',
        type: 'GET',
        data: { 'page': page },
        error: function (ex) {
            console.log(ex);
        },
        success: function (response) {
            if (response.Results != null) {
                var jsonResponse = response.Results;
                console.log(jsonResponse);
                var tableStr = "";
                for (var i = 0; i < jsonResponse.length; i++) {
                    tableStr += '<tr>';
                    tableStr += '<td class="hidden">' + jsonResponse[i].IdProduct + '</td>';
                    tableStr += '<td>' + jsonResponse[i].Name + '</td>';
                    tableStr += '<td>' + jsonResponse[i].Price + '</td>';
                    tableStr += '<td>' + jsonResponse[i].Stock + '</td>';
                    tableStr += '</tr>';
                }
                $('#modalNewBillAddProductsTable > tbody ').append(tableStr);
            }
        }
    });
}

$(document).ready(function () {
    $(function () {
        $('#modalNewBillAddProductsTable tbody').on('click', 'tr', function () {
            var idProduct = $(this).find('td.hidden').html();
            $.ajax({
                url: '/bills/AddSelectedProduct',
                type: 'GET',
                data: { productId: idProduct },
                error: function (ex) {
                    console.log(ex);
                },
                success: function (response) {
                    if (response.Results != null) {
                        var jsonResponse = response.Results;
                        var tableStr = "";
                        for (var i = 0; i < jsonResponse.length; i++) {
                            tableStr += '<tr>';
                            tableStr += '<td class="hidden">' + jsonResponse[i].IdProduct + '</td>';
                            tableStr += '<td>' + jsonResponse[i].Name + '</td>';
                            tableStr += '<td>' + jsonResponse[i].Price + '</td>';
                            tableStr += '<td>' + jsonResponse[i].Stock + '</td>';
                            tableStr += '</tr>';
                            $('#modalNewBillSelectedProductsTable > tbody ').append(tableStr);
                        }
                    }
                }
            });
        });
    });
});

$(document).ready(function () {
    $(function () {
        $('#btnModalNewBillAddDetail').on('click', function () {
            var items = [];
            var arrIdProduct = [];
            var arrProductName = [];
            var arrPrice = [];
            var arrStock = [];

            $('#modalNewBillSelectedProductsTable tbody tr td:nth-child(1)').each(function () {
                var idProduct = $(this).text();
                arrIdProduct.push(idProduct);
            });
            $('#modalNewBillSelectedProductsTable tbody tr td:nth-child(2)').each(function () {
                var productName = $(this).text();
                arrProductName.push(productName);
            });
            $('#modalNewBillSelectedProductsTable tbody tr td:nth-child(3)').each(function () {
                var price = $(this).text();
                arrPrice.push(price);
            });
            $('#modalNewBillSelectedProductsTable tbody tr td:nth-child(4)').each(function () {
                var stock = $(this).text();
                arrStock.push(stock);
            });

            for (var i = 0; i < arrIdProduct.length; i++) {
                items.push(arrIdProduct[i], arrProductName[i], arrPrice[i], arrStock[i]);
            }

            $.ajax({
                url: '/bills/CreateNewDetail',
                type: 'POST',
                data: { items: items },
                error: function (ex) {
                    console.log(ex);
                },
                success: function (response) {
                    if (response.Results != null) {
                        var jsonResponse = response.Results;
                        var tableStr = "";
                        for (var i = 0; i < jsonResponse.length; i++) {
                            tableStr = '<tr>';
                            tableStr += '<td class="hidden">' + jsonResponse[i].IdProduct + '</td>';
                            tableStr += '<td>' + jsonResponse[i].ProductName + '</td>';
                            tableStr += '<td>' + jsonResponse[i].Quantity + '</td>';
                            tableStr += '<td>' + jsonResponse[i].TotalDetail + '</td>';
                            tableStr += '</tr>';
                            $('#modalNewBillSelectedDetailsAdded > tbody ').append(tableStr);
                            detailAdded = true;
                        }
                    }
                }
            });
            $('#modalNewBillSelectedProductsTable tr td').each(function () {
                $(this).remove();
            });
        });
    });
});

$(document).ready(function () {
    var partial = "";
    var jsonResponse = "";
    $('#modalNewBillOpen').on('click', function () {
        $('#partialSearch').on('keyup', function (event) {
            $('#clientSearch option').each(function () {
                $(this).remove();
            });
            partial = $(this).val();
            $.ajax({
                url: '/bills/GetClients',
                data: { 'partial': partial },
                dataType: "json",
                type: "GET",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    if (response.Results != null) {
                        jsonResponse = response.Results;
                        if (partial.length >= 1) {
                            var optionLenght = jsonResponse.length;
                            if (optionLenght > 5) optionLenght = 5;
                            for (var i = 0; i < optionLenght; i++) {
                                var optionData = "<option value='" + jsonResponse[i].IdClient + "'>" + jsonResponse[i].ClientName + "</option>";
                                $('#clientSearch').append(optionData);
                                $('#clientSearch').show().fadeIn(1000);
                            }
                        } else {
                            $('#clientSearch').hide("fade", {}, 1000);
                        }
                    }
                }
            });
        });
    });

    //create new Bill

    $('datalist#clientSearch').on('click', 'option', function () {
        var optionSelId = $(this).val();
        var optionSelText = $(this).text();
        $('#partialSearch').val(optionSelText);
        $('#clientSearch').hide();
        $.ajax({
            url: '/bills/GetClient',
            data: { optionSelId },
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                if (response.Results != null) {
                    var jsonResponseCard = response.Results;
                    for (var i = 0; i < jsonResponse.length; i++) {
                        $('#clientCard #cardTitleText').text(jsonResponseCard[i].ClientName);
                        $('#clientCard #cardTextClientId').text(jsonResponseCard[i].IdClient);
                        $('#clientCard #cardTextAddress').text(jsonResponseCard[i].ClientAddress);
                        $('#clientCard #cardTextMail').text(jsonResponseCard[i].ClientEmail);
                        clientSelected = true;
                    }
                }
            }
        });
    });

    $(document).ready(function () {
        $("#phone").mask("(999)999-9999");
    });

    $(document).ready(function () { //firstName lastName email phone address newClientModalBtn
        $('#newClientModalBtn').on('click', function () {
            if ($('#firstName').val() != "" && $('#lastName').val() != "" && $('#email').val() != "" && $('#phone').val() != "" && $('#address').val() != "") {
                var client = [];
                client.push($('#firstName').val());
                client.push($('#lastName').val());
                client.push($('#email').val());
                client.push($('#address').val());
                client.push($('#phone').val());
                $.ajax({
                    url: '/clients/AddNewClient',
                    dataType: "json",
                    data: JSON.stringify(client),
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        if (response != null) {
                            if (response === true) {
                                alert("Client created succesfully!!");
                                $('#newClientModal').modal('hide');
                            } else {
                                alert("Something went wrong!!");
                                $('#newClientModal').modal('hide');
                            }
                        }
                    }
                });
            }
        });
    });

    $(document).ready(function () {
        $('#btnModalNewBillClose').on('click', function () {
            window.location.reload();
        });
    });


    $(document).ready(function () {
        $("#btnModalNewBillClose").on("hidden.bs.modal", function () {
            window.location.reload();
        });
    });

    $(document).ready(function () {

        $('#btnModalNewBillValidate').on('click', function () {
            if (clientSelected === true && detailAdded === true) {
                $('#btnModalNewBillCreate').removeClass('disabled');
            }
        });

    });

    $(document).ready(function () {
        $('#btnModalNewBillCreate').on('click', function () {
            //$('#modalNewBillSelectedDetailsAdded');
            var items = [];
            var arrIdProduct = [];
            var arrProductName = [];
            var arrQuantity = [];
            var arrTotal = [];

            $('#modalNewBillSelectedDetailsAdded tbody tr td:nth-child(1)').each(function () {
                var idProduct = $(this).text();
                arrIdProduct.push(idProduct);
            });
            $('#modalNewBillSelectedDetailsAdded tbody tr td:nth-child(2)').each(function () {
                var productName = $(this).text();
                arrProductName.push(productName);
            });
            $('#modalNewBillSelectedDetailsAdded tbody tr td:nth-child(3)').each(function () {
                var quantity = $(this).text();
                arrQuantity.push(quantity);
            });
            $('#modalNewBillSelectedDetailsAdded tbody tr td:nth-child(4)').each(function () {
                var total = $(this).text();
                arrTotal.push(total);
            });

            for (var i = 0; i < arrIdProduct.length; i++) {
                items.push(arrIdProduct[i], arrProductName[i], arrQuantity[i], arrTotal[i]);
            }

            var clientId = $('#cardTextClientId').text();
            var billNumber=0;
            
            $.ajax({
                url: '/bills/SaveBill',
                type: 'POST',
                data: { clientId: clientId, items: items},
                error: function (ex) {
                    console.log(ex);
                },
                success: function (response) {
                    if (response.Results != null) {
                        billNumber = response.Results;
                        $.ajax({
                            url: '/bills/SaveDetail',
                            type: 'POST',
                            data: { billNumber: billNumber, items: items },
                            error: function (ex) {
                                console.log(ex);
                            },
                            success: function (response) {
                                if (response.Results != null) {
                                    alert("bill saved");
                                    $('#modalNewBillSelectedDetailsAdded tr td').each(function () {
                                        $(this).remove();
                                    });
                                    $('#btnModalNewBillCreate').addClass('disabled');
                                }
                            }
                        });
                    }
                }
            });
        });
    });
});

