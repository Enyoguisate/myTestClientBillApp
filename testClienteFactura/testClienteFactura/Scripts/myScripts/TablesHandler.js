﻿

$(document).ready(function () {
   $(function () {
        $('#ClientsDataTable > tbody > tr').click(function () {
           var id = this.cells[0].textContent;
            $.ajax({
                url: '/home/GetBillById',
                type: 'GET',
                data: { billId: id },
                error: function (ex) {
                    console.log(ex);
                },
                success: function (response) {
                    if (response.Results != null) {
                        var jsonResponse = response.Results;
                        var tableStr = "";
                        for (var i = 0; i < jsonResponse.length; i++) {
                            tableStr += '<tr>';
                            tableStr += '<td>' + jsonResponse[i].ClientName + '</td>';
                            tableStr += '<td>' + jsonResponse[i].BillNumber + '</td>';
                            tableStr += '<td>' + jsonResponse[i].BillDateFormated + '</td>';
                            tableStr += '<td>' + jsonResponse[i].BillTotal + '</td>';
                            tableStr += '</tr>';
                            $('#BillsDataTable > tbody ').append(tableStr);
                        }
                    }
                }
            });
        });
    });
});


//Load Detail table script
$(document).on('click', '#BillsDataTable > tbody > tr', function () {
    var billNumber = this.cells[1].textContent;
    $.ajax({
        url: '/home/GetDetailsByBillNumber',
        type: 'GET',
        data: { billNumber: billNumber },
        error: function (ex) {
            console.log(ex);
        },
        success: function (response) {
            if (response.Results != null) {
                var jsonResponse = response.Results;
                console.log(jsonResponse);
                var tableStr = "";
                for (var i = 0; i < jsonResponse.length; i++) {
                    tableStr += '<tr>';
                    tableStr += '<td>' + jsonResponse[i].BillNumber + '</td>';
                    tableStr += '<td>' + jsonResponse[i].DetailNumber + '</td>';
                    tableStr += '<td>' + jsonResponse[i].IdProduct + '</td>';
                    tableStr += '<td>' + jsonResponse[i].ProductName + '</td>';
                    tableStr += '<td>' + jsonResponse[i].Quantity + '</td>';
                    tableStr += '<td>' + jsonResponse[i].TotalDetail + '</td>';
                    tableStr += '</tr>';
                }
                $('#DetailDataTable > tbody ').append(tableStr);
            }
        }
    });
});


$(document).on('click', '#DetailDataTable > tbody > tr', function () {
    var idProduct = this.cells[2].textContent;
    $.ajax({
        url: '/home/GetProductsByIdProduct',
        type: 'GET',
        data: { idProduct: idProduct },
        error: function (ex) {
            console.log(ex);
        },
        success: function (response) {
            if (response.Results != null) {
                var jsonResponse = response.Results;
                console.log(jsonResponse);
                var tableStr = "";
                for (var i = 0; i < jsonResponse.length; i++) {
                    tableStr += '<tr>';
                    tableStr += '<td>' + jsonResponse[i].IdProduct + '</td>';
                    tableStr += '<td>' + jsonResponse[i].Name + '</td>';
                    tableStr += '<td>' + jsonResponse[i].Price + '</td>';
                    tableStr += '<td>' + jsonResponse[i].Stock + '</td>';
                    tableStr += '</tr>';
                }
                $('#ProductsDataTable > tbody ').append(tableStr);
            }
        }
    });
});

$(document).ready(function() {
    var clientId
    $('#ClientsDataTable > tbody > tr > td > #modalOpen').on('click', function () {
        clientId = this.name;
        $.ajax({
            url: '/bills/GetBillsByClientId',
            type: 'GET',
            data: { clientId: this.name },
            error: function (ex) {
                console.log(ex);
            },
            success: function (response) {
                if (response.Results != null) {
                    var jsonResponse = response.Results;
                    console.log(jsonResponse);
                    var tableStr = "";
                    for (var i = 0; i < jsonResponse.length; i++) {
                        tableStr += '<tr>';
                        tableStr += '<td class="hidden">' + jsonResponse[i].IdClient + '</td>';
                        tableStr += '<td>' + jsonResponse[i].ClientName + '</td>';
                        tableStr += '<td>' + jsonResponse[i].BillNumber + '</td>';
                        tableStr += '<td>' + jsonResponse[i].BillDateFormated + '</td>';
                        tableStr += '<td>' + jsonResponse[i].BillTotal + '</td>';
                        tableStr += '</tr>';
                    }
                    $('#BillsModalDataTable > tbody ').append(tableStr);
                }
            }
        });
    });

    $('#modalDeleteBtn').on('click', function () {
        $.ajax({
            url: '/bills/DeleteByClientId',
            type: 'GET',
            data: { idClient: clientId },
            error: function (ex) {
                console.log(ex);
            },
            success: function (response) {
                window.location.reload();
            }
        });
    });

    $('#modalCloseBtn').on('click', function () {
        window.location.reload();
    });

    $("#deleteModal").on("hidden.bs.modal", function () {
        window.location.reload();
    });
});

$(document).ready(function() {
    $('#BillsDataTable > tbody > tr > td > #billModalOpen').on('click', function () {
        var numberBill = this.name;
        $.ajax({
            url: '/bills/GetBillsByBillNumber',
            type: 'GET',
            data: { billNumber: numberBill },
            error: function (ex) {
                console.log(ex);
            },
            success: function (response) {
                if (response.Results != null) {
                    var jsonResponse = response.Results;
                    console.log(jsonResponse);
                    var tableStr = "";
                    for (var i = 0; i < jsonResponse.length; i++) {
                        tableStr += '<tr>';
                        tableStr += '<td>' + jsonResponse[i].BillNumber + '</td>';
                        tableStr += '<td>' + jsonResponse[i].DetailNumber + '</td>';
                        tableStr += '<td>' + jsonResponse[i].IdProduct + '</td>';
                        tableStr += '<td>' + jsonResponse[i].Quantity + '</td>';
                        tableStr += '<td>' + jsonResponse[i].TotalDetail + '</td>';
                        tableStr += '</tr>';
                    }
                    $('#DetailModalDataTable > tbody ').append(tableStr);
                }
            }
        });

    });
});


$(document).ready(function() {
    $('#detailModalDeleteBtn').on('click', function () {
        $.ajax({
            url: '/bills/DeleteBillByBillNumber',
            type: 'GET',
            data: { billNumber: numberBill },
            error: function (ex) {
                console.log(ex);
            },
            success: function (response) {
                window.location.reload();
            }
        });

    });
});


$(document).ready(function() {
    $('#detailModalCloseBtn').on('click', function () {
        window.location.reload();
    });
});


$(document).ready(function() {
    $("#deleteBillModal").on("hidden.bs.modal", function () {
        window.location.reload();
    });
});




