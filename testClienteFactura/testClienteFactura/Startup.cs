﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(testClienteFactura.Startup))]
namespace testClienteFactura
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
