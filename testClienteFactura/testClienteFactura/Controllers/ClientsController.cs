﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testClientBillBLL;
using Clients = testClientBillModels.Clients;
namespace testClienteFactura.Controllers
{
    public class ClientsController : Controller
    {
        private testClientBillBLL.Clients _ClientBll = new testClientBillBLL.Clients();

        //********************************************
        //          Show Clients List
        //********************************************
        public ActionResult List()
        {
            return View(_ClientBll.GetClientList(0));
        }

        //********************************************
        //          Show Clients Add Form
        //********************************************
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        //********************************************
        //              Add Client
        //********************************************
        [HttpPost]
        public ActionResult AddClient(Clients client)
        {
            client.ClientCreated = DateTime.Today;
            client.ClientLastAccess = DateTime.Today;
            _ClientBll.RegisterClient(client);
            return View("List",_ClientBll.GetClientList(0));
        }

        public class ClientMock 
        {
            public string ClientFirstName { get; set; }
            public string ClientLastName { get; set; }
            public string ClientEmailAddress { get; set; }
            public string ClientPhone { get; set; }
            public string ClientAddress { get; set; }
            
        }

        [HttpPost]
        public JsonResult AddNewClient(string[] client)
        {

            testClientBillModels.Clients newClient = new Clients();
            newClient.ClientFirstName = client[0].ToUpper();
            newClient.ClientLastName = client[1].ToUpper();
            newClient.ClientEmail = client[2].ToUpper();
            newClient.ClientPhone = client[4];
            newClient.ClientAddress = client[3].ToUpper();
            newClient.ClientCreated = DateTime.Today;
            newClient.ClientLastAccess = DateTime.Today;
            bool result = _ClientBll.RegisterClient(newClient);
            return Json(result,JsonRequestBehavior.AllowGet);
        }

        //********************************************
        //          Show Clients Edit Form
        //********************************************
        [HttpGet]
        public ActionResult Edit(Clients client)
        {
            return View(client);
        }

        //********************************************
        //          Edit Client
        //********************************************
        [HttpPost]
        public ActionResult ChangeClient(Clients client)
        {
            if (client != null)
            {
                client.ClientLastAccess = DateTime.Today;
                _ClientBll.ChangeClient(client);
            }
            return View("List",_ClientBll.GetClientList(0));
        }

        //********************************************
        //          Delete Client
        //********************************************
      
        [HttpGet]
        public ActionResult Delete(int idClient)
        {
            if (idClient!=0)_ClientBll.RemoveClient(idClient);
            return View("List",_ClientBll.GetClientList(0));
        }

    }
}