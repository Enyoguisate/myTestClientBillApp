﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testClientBillBLL;
using Bills = testClientBillModels.Bills;


namespace testClienteFactura.Controllers
{
    public class HomeController : Controller
    {
        testClientBillBLL.Clients _ClientsBll = new Clients();
        testClientBillBLL.Bills _BillsBll = new testClientBillBLL.Bills();
        testClientBillBLL.Details _DetailsBll = new Details();
        testClientBillBLL.Products _ProductBll = new Products();
        
        public ActionResult Index()
        {
            return View(_ClientsBll.GetClientList(0));
        }

        [HttpGet]
        public JsonResult GetBillById(int billId)
        {
            List<Bills> bills = _BillsBll.GetBillsListByClientId(billId);
            return Json(new {Results = bills}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDetailsByBillNumber(int billNumber)
        {
            List<testClientBillModels.Detail> details = _DetailsBll.GetDetailsList(billNumber);
            return Json(new { Results = details }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetProductsByIdProduct(int idProduct)
        {
            List<testClientBillModels.Products> products = _ProductBll.GetProductsById(idProduct);
            return Json(new { Results = products }, JsonRequestBehavior.AllowGet);
        }
        

    }
}