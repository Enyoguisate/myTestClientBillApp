﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testClientBillModels
{
    public class Users
    {
        [Required]
        [Display(Name = "User name")]
        public String UserUsername { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public String UserPassword { get; set; }

        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; }

    }
}
