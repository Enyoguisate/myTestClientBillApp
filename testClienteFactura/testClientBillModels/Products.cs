﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testClientBillModels
{
    public class Products
    {
        public int IdProduct { get; set; }
        public string Name { get; set; }

        public Decimal Price { get; set; }

        public int Stock{ get; set; }
    }
}
