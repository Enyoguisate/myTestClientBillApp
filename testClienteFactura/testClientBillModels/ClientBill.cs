﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testClientBillModels
{
    public class ClientBill
    {
        //Client
        public string ClientFirstName { get; set; }
        public string ClientLastName { get; set; }
        public string ClientEmail { get; set; }
        public string ClientPhone { get; set; }
        public string ClientAddress { get; set; }
        public DateTime ClientCreated { get; set; }
        public string ClientCreatedFormated => ClientCreated.ToShortDateString();
        public DateTime ClientLastAccess { get; set; }
        public string ClientLastAccessFormated => ClientLastAccess.ToShortDateString();

        public int IdClient { get; set; }
        public int IdBill { get; set; }

        //Bill
        public DateTime BillDate { get; set; }
        public string BillDateFormated => BillDate.ToShortDateString();
        public Decimal BillTotal { get; set; }

        public int BillNumber { get; set; }

        //Detail
        public int DetailNumber { get; set; }
        public int Quantity { get; set; }
        public decimal TotalDetail { get; set; }

        public int IdProduct { get; set; }

        //Products
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
        
    }
}
