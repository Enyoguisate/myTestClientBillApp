﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testClientBillModels;

namespace testClientBillDAL
{
    public class Bills : Database
    {
        public Bills() : base()
        {
        }

        /*
         SP_GETBILLSBYID
         SP_GETBILLSBYCLIENTID
             SP_GETCLIENTBILLBYID
             
             */


        public List<testClientBillModels.ClientBill> GetClientBillsById(int idClient, int idBill, int op)
        {
            List<testClientBillModels.ClientBill> clientBillsList = new List<ClientBill>();
            string[] spTo = { "[SP_GETCLIENTBILLBYID]", "[SP_GETCLIENTSBILLS]" };
            try
            {
                using (var cmd = GetCommand(spTo[op], CommandType.StoredProcedure))
                {
                    
                        if (op == 0)
                        {
                            cmd.Parameters.AddWithValue("@idBill", idBill);
                            cmd.Parameters.AddWithValue("@idClient", idClient);
                        }
                        using (var reader = cmd.ExecuteReader())
                        {
                            if (!reader.HasRows) return null;
                            int idxClientFirstName = reader.GetOrdinal("clientFirstName");
                            int idxClientLastName = reader.GetOrdinal("clientLastName");
                            int idxClientEmail = reader.GetOrdinal("clientEmail");
                            int idxClientPhone = reader.GetOrdinal("clientPhone");
                            int idxClientAddress = reader.GetOrdinal("clientAddress");
                            int idxClientCreated = reader.GetOrdinal("clientCreated");
                            int idxClientLastAccess = reader.GetOrdinal("clientLastAccess");
                            int idxIdClient = reader.GetOrdinal("idClient");
                            int idxIdBill = reader.GetOrdinal("idBill");
                            int idxBillDate = reader.GetOrdinal("billDate");
                            int idxBillTotal = reader.GetOrdinal("billTotal");
                            int idxBillNumber = reader.GetOrdinal("billNumber");
                            int idxDetailNumber = reader.GetOrdinal("detailNumber");
                            int idxQuantity = reader.GetOrdinal("quantity");
                            int idxTotalDetail = reader.GetOrdinal("TotalDetail");
                            int idxIdProduct = reader.GetOrdinal("idProduct");
                            int idxName = reader.GetOrdinal("name");
                            int idxPrice = reader.GetOrdinal("price");
                            int idxStock = reader.GetOrdinal("stock");
                            while (reader.Read())
                            {
                                testClientBillModels.ClientBill clientBill = new testClientBillModels.ClientBill();

                                clientBill.ClientFirstName = reader.GetString(idxClientFirstName);
                                clientBill.ClientLastName = reader.GetString(idxClientLastName);
                                clientBill.ClientEmail = reader.GetString(idxClientEmail);
                                clientBill.ClientPhone = reader.GetString(idxClientPhone);
                                clientBill.ClientAddress = reader.GetString(idxClientAddress);
                                clientBill.ClientCreated = reader.GetDateTime(idxClientCreated);
                                clientBill.ClientLastAccess = reader.GetDateTime(idxClientLastAccess);
                                clientBill.IdClient = reader.GetInt32(idxIdClient);
                                clientBill.IdBill = reader.GetInt32(idxIdBill);
                                clientBill.BillDate = reader.GetDateTime(idxBillDate);
                                clientBill.BillTotal = reader.GetDecimal(idxBillTotal);
                                clientBill.BillNumber = reader.GetInt32(idxBillNumber);
                                clientBill.DetailNumber = reader.GetInt32(idxDetailNumber);
                                clientBill.Quantity = reader.GetInt32(idxQuantity);
                                clientBill.TotalDetail = reader.GetDecimal(idxTotalDetail);
                                clientBill.IdProduct = reader.GetInt32(idxIdProduct);
                                clientBill.Name = reader.GetString(idxName);
                                clientBill.Price = reader.GetDecimal(idxPrice);
                                clientBill.Stock = reader.GetInt32(idxStock);

                                clientBillsList.Add(clientBill);
                            }
                        }
                    }

                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return clientBillsList;
        }

        public List<testClientBillModels.Bills> GetBillsById(int idBill, int idClient, int op)
        {
            string[] spTo = {"[SP_GETBILLSBYID]", "[SP_GETBILLSBYCLIENTID]"};
            List<testClientBillModels.Bills> billsList = new List<testClientBillModels.Bills>();
            try
            {
                using (var cmd = GetCommand(spTo[op], CommandType.StoredProcedure))
                {
                    if (op == 0 && idBill != 0) cmd.Parameters.AddWithValue("@idBill", idBill);
                    if (op == 1 && idClient != 0) cmd.Parameters.AddWithValue("@idClient", idClient);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows) return null;
                        int idxIdBill = reader.GetOrdinal("idBill");
                        int idxBillDate = reader.GetOrdinal("billDate");
                        int idxBillNumber = reader.GetOrdinal("billNumber");
                        int idxBillTotal = reader.GetOrdinal("billTotal");
                        int idxIdClient = reader.GetOrdinal("idClient");
                        int idxClientName = reader.GetOrdinal("clientName");
                        while (reader.Read())
                        {
                            testClientBillModels.Bills bill = new testClientBillModels.Bills();
                            bill.IdBill = reader.GetInt32(idxIdBill);
                            bill.BillDate = reader.GetDateTime(idxBillDate);
                            bill.BillNumber = reader.GetInt32(idxBillNumber);
                            bill.BillTotal = reader.GetDecimal(idxBillTotal);
                            bill.IdClient = reader.GetInt32(idxIdClient);
                            bill.ClientName = reader.GetString(idxClientName);
                            billsList.Add(bill);
                        }
                        return billsList;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public bool InsertBill(testClientBillModels.Bills bill)
        {
            bool aState = false;
            try
            {
                using (var cmd = GetCommand("[SP_INSBILL]", CommandType.StoredProcedure))
                {
                    cmd.Parameters.AddWithValue("@billDate", bill.BillDate);
                    cmd.Parameters.AddWithValue("@billNumber", bill.BillNumber);
                    cmd.Parameters.AddWithValue("@billTotal", bill.BillTotal);
                    cmd.Parameters.AddWithValue("@idClient", bill.IdClient);
                    cmd.ExecuteNonQuery();
                    aState = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return aState;
        }

        public bool UpdateBill(testClientBillModels.Bills bill)
        {
            try
            {
                var reader = 0;
                using (var cmd = GetCommand("[SP_UPDBILL]", CommandType.StoredProcedure))
                {
                    cmd.Parameters.AddWithValue("@idBill", bill.IdBill);
                    cmd.Parameters.AddWithValue("@billDate", bill.BillDate);
                    cmd.Parameters.AddWithValue("@billNumber", bill.BillNumber);
                    cmd.Parameters.AddWithValue("@billTotal", bill.BillTotal);
                    cmd.Parameters.AddWithValue("@idClient", bill.IdClient);
                    reader = cmd.ExecuteNonQuery();
                }
                return (reader != 0);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public bool DeleteBillById(int id)
        {
            var aState = false;
            try
            {
                using (var cmd = GetCommand("[SP_DELBILLBYID]", CommandType.StoredProcedure))
                {
                    cmd.Parameters.AddWithValue("@idBill", id);
                    var reader = cmd.ExecuteNonQuery();
                    if (reader < 0)
                    {
                        aState = true;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return aState;
        }

        public int GetLastBillNumber()
        {
            int id = 0;
            string pk = "billNumber";
            string table = "Bills";
            try
            {
                StringBuilder query = new StringBuilder();
                query.AppendFormat("select max(" + pk + ") as maxid from " + table, pk, table);
                using (var cmd = GetCommand(query.ToString(), CommandType.Text))
                {
                    var reader = cmd.ExecuteReader();
                    if (!reader.HasRows) return id;
                    int maxID = reader.GetOrdinal("MAXID");
                    while (reader.Read())
                    {
                        id = reader.GetInt32(maxID) + 1;
                        return id;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return id;
        }
    }
}
