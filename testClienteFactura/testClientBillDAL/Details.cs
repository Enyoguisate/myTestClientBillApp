﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testClientBillModels;

namespace testClientBillDAL
{
    public class Details : Database
    {
        public Details() : base()
        {
        }

        public List<testClientBillModels.Detail> GetDetailsById(int billNumber)
        {
            List<testClientBillModels.Detail> detailsList = new List<Detail>();
            try
            {
                using (var cmd = GetCommand("[SP_GETDETAILSBYBILLNUMBER]", CommandType.StoredProcedure))
                {
                    if(billNumber!=0)cmd.Parameters.AddWithValue("@billNumber", billNumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows) return null;
                        int idxDetailNumber = reader.GetOrdinal("detailNumber");
                        int idxBillNumber = reader.GetOrdinal("billNumber");
                        int idxIdProduct = reader.GetOrdinal("idProduct");
                        int idxQuantity = reader.GetOrdinal("quantity");
                        int idxTotalDetail = reader.GetOrdinal("TotalDetail");
                        int idxProductName = reader.GetOrdinal("name");
                        while (reader.Read())
                        {
                            testClientBillModels.Detail detail = new Detail();
                            detail.DetailNumber = reader.GetInt32(idxDetailNumber);
                            detail.BillNumber = reader.GetInt32(idxBillNumber);
                            detail.IdProduct = reader.GetInt32(idxIdProduct);
                            detail.Quantity = reader.GetInt32(idxQuantity);
                            detail.TotalDetail = reader.GetDecimal(idxTotalDetail);
                            detail.ProductName = reader.GetString(idxProductName);
                            detailsList.Add(detail);
                        }
                        return detailsList;
                    }
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public bool DeleteDetailByBillNumber(int billNumber)
        {
            var aState = false;
            try
            {
                using (var cmd = GetCommand("[SP_DELDETAILSBYBILLNUMBER]", CommandType.StoredProcedure))
                {
                    if(billNumber!=0)cmd.Parameters.AddWithValue("@billNumber", billNumber);
                    var reader = cmd.ExecuteNonQuery();
                    if (reader < 0)
                    {
                        aState = true;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return aState;
        }
        
        public bool InsertDetail(testClientBillModels.Detail detail)
        {
            bool aState = false;
            try
            {
                using (var cmd = GetCommand("[SP_INSDETAIL]", CommandType.StoredProcedure))
                {
                    cmd.Parameters.AddWithValue("@billNumber", detail.BillNumber);
                    cmd.Parameters.AddWithValue("@idProduct", detail.IdProduct);
                    cmd.Parameters.AddWithValue("@quantity", detail.Quantity);
                    cmd.Parameters.AddWithValue("@TotalDetail", detail.TotalDetail);
                    cmd.ExecuteNonQuery();
                    aState = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return aState;
        }


    }
}
