﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testClientBillDAL
{
    public class Products : Database
    {
        public Products(): base(){ }
    
        public List<testClientBillModels.Products> GetProductsById(int idProduct)
        {
            List<testClientBillModels.Products> productsList = new List<testClientBillModels.Products>();
            try
            {
                using (var cmd = GetCommand("[SP_GETPRODUCTSBYID]", CommandType.StoredProcedure))
                {
                    if(idProduct!=0)cmd.Parameters.AddWithValue("@idProduct", idProduct);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows) return null;
                        int idxIdProduct = reader.GetOrdinal("idProduct");
                        int idxName = reader.GetOrdinal("name");
                        int idxPrice = reader.GetOrdinal("price");
                        int idxStock = reader.GetOrdinal("stock");
                        
                        while (reader.Read())
                        {
                            testClientBillModels.Products product = new testClientBillModels.Products();
                            product.IdProduct = reader.GetInt32(idxIdProduct);
                            product.Name = reader.GetString(idxName);
                            product.Price = reader.GetDecimal(idxPrice);
                            product.Stock = reader.GetInt32(idxStock);
                            productsList.Add(product);
                        }
                        return productsList;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }

        }
    }
}

