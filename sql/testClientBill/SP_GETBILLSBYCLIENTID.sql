USE [testClientBill]
GO
/****** Object:  StoredProcedure [dbo].[SP_GETBILLSBYCLIENTID]    Script Date: 05-Aug-17 7:44:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_GETBILLSBYCLIENTID]
	-- Add the parameters for the stored procedure here
	@idClient int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT b.idBill, b.billDate, b.billTotal,b.idClient,b.billNumber,
	c.clientFirstName +' '+c.clientLastName as clientName
	FROM Bills b
	inner join Clients c on(c.idClient=b.idClient)
	WHERE
	(@idClient is null or @idClient = b.idClient)
END

