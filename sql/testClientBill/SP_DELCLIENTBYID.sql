USE [testClientBill]
GO
/****** Object:  StoredProcedure [dbo].[SP_INSCLIENTS]    Script Date: 02-Aug-17 10:54:33 AM ******/

-- =============================================
-- Author:		Victor Martin
-- Create date: 02/08/2017
-- Description:	This SP is use to delete a client
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_DELCLIENTBYID]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_DELCLIENTBYID] as return -1')
go
alter PROCEDURE SP_DELCLIENTBYID
	-- Add the parameters for the stored procedure here
	@idClient int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DELETE FROM Clients
	WHERE
	(@idClient = idClient)
END
GO
