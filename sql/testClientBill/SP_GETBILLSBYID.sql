USE [testClientBill]
GO
/****** Object:  StoredProcedure [dbo].[SP_GETBILLSBYID]    Script Date: 05-Aug-17 7:44:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_GETBILLSBYID]
	-- Add the parameters for the stored procedure here
	@idBill int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT b.*, c.clientFirstName +' '+c.clientLastName as clientName FROM Bills b
	inner join Clients c on(c.idClient = b.idClient)
	WHERE
	(@idBill is null or @idBill=idBill)
END

