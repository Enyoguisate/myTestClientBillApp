USE [testClientBill]
GO
/****** Object:  StoredProcedure [dbo].[SP_INSCLIENTS]    Script Date: 02-Aug-17 10:32:27 AM ******/

-- =============================================
-- Author:		Victor Martin
-- Create date: 02/08/2017
-- Description:	This SP is use to insert a client
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_INSCLIENTS]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_INSCLIENTS] as return -1')
go
alter PROCEDURE SP_INSCLIENTS
	-- Add the parameters for the stored procedure here
	@clientFirstName varchar(50) = null,
	@clientLastName varchar(50) = null,
	@clientEmail varchar(50) = null,
	@clientPhone varchar(50) = null,
	@clientAddress varchar(50) = null,
	@clientCreated datetime = null,
	@clientLastAccess datetime = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into Clients(clientFirstName,clientLastName,clientEmail,clientPhone,clientAddress,clientCreated,clientLastAccess)
	values(@clientFirstName,@clientLastName,@clientEmail,@clientPhone,@clientAddress,@clientCreated,@clientLastAccess)
END
