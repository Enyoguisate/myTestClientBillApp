USE [testClientBill]
GO
/****** Object:  StoredProcedure [dbo].[SP_INSBILL]    Script Date: 02-Aug-17 11:23:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 02/08/2017
-- Description:	this sp is use to insert a bill
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_INSBILL]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_INSBILL] as return -1')
go
ALTER PROCEDURE [dbo].[SP_INSBILL]
	-- Add the parameters for the stored procedure here
	@billDate datetime = null,
	@billTotal numeric(18,2) = null,
	@billNumber int = null,
	@idClient int = null	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into Bills(billDate,billTotal,billNumber,idClient)
	values(@billDate,@billTotal,@billNumber,@idClient)
END
