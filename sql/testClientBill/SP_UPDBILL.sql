USE [testClientBill]
GO
/****** Object:  StoredProcedure [dbo].[SP_UPDBILL]    Script Date: 02-Aug-17 11:32:01 AM ******/

-- =============================================
-- Author:		Victor Martin
-- Create date: 02/08/2017
-- Description:	This SP is use to update a bill
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_UPDBILL]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_UPDBILL] as return -1')
go
alter PROCEDURE SP_UPDBILL
	-- Add the parameters for the stored procedure here
	@idBill int,
	@billDate datetime = null,
	@billTotal numeric(18,2) = null,
	@billNumber int = null,
	@idClient int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Bills
	set
	billDate = ISNULL(@billDate, billDate),
	billTotal = ISNULL(@billTotal, billTotal),
	billNumber = ISNULL(@billNumber, billNumber),
	idClient = ISNULL(@idClient, idClient)
	WHERE
	(@idBill is null or @idBill = idBill)
END


